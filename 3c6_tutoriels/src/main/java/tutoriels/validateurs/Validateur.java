/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package tutoriels.validateurs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import ca.ntro.core.NtroCore;
import ca.ntro.core.UnitializedNtro;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraph;



public class Validateur {

	static boolean continuerDeValider = true;

	static boolean ecrireGraphes = true;

	static FileWriter htmlOut = null;
	
	static Random alea = new Random();

	static String testName(int testId) {
		return String.format("test%02d", testId);
	}

	static void htmlTestName(String testName) {
		try {
			htmlOut.write("<h1>" + testName + "</h1>");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	static void htmlTestPage(StringBuilder page) {
		htmlTestPage(page.toString());
	}

	static void htmlTestPage(String page) {
		try {
			htmlOut.write(page.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static File htmlFile(Object object) {
		String htmlFilename = UnitializedNtro.reflection().simpleName(object.getClass()) + ".html";

		return htmlFileFromFilename(htmlFilename);
	}

	private static File htmlFileFromFilename(String htmlFilename){
		Path htmlPath = Paths.get(NtroCore.options().projectPath(), htmlFilename);

		return htmlPath.toFile();
	}

	static File htmlFile(Object object, String forcedClassName) {
		String htmlFilename = "";

		if(forcedClassName != null) {
			htmlFilename = forcedClassName + ".html";

			return htmlFileFromFilename(htmlFilename);
			
		}else {

			return htmlFile(object);
		}
	}

	static String graphName(Object object, String testName, String suffix) {
		return UnitializedNtro.reflection().simpleName(object.getClass()) + "_" + testName + "_" + suffix;
	}

	static void writeObjectGraph(Object object, String graphName) {
		if(!ecrireGraphes) return;

		ObjectGraph graph = UnitializedNtro.reflection().graphFromObject(object, graphName);
		graph.write(UnitializedNtro.graphWriter());
	}
	
	static String graphSrc(String graphName) {

		Path graphPath = Paths.get(NtroCore.options().graphsPath(), graphName + ".png");
		
		String graphSrc = "";
		

		try {

			graphSrc = graphPath.toUri().toURL().toString();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return graphSrc;
	}

	static void cssTable(StringBuilder page) {
		page.append("<style>");
		page.append("table{border:1px solid black;}");
		page.append(System.lineSeparator());
		page.append("tr{border:1px solid black;}");
		page.append(System.lineSeparator());
		page.append("td{border:1px solid black;}");
		page.append(System.lineSeparator());
		page.append("th{border:1px solid black;}");
		page.append("</style>");

	}

	static void tablePourValeurDeRetour(StringBuilder page) {
		cssTable(page);
		page.append("<table>");
		page.append("<tr>");
		page.append("<th>");
		page.append("Valeur avant opération");
		page.append("</th>");
		page.append("<th>");
		page.append("Opération");
		page.append("</th>");
		page.append("<th>");
		page.append("Valeur de retour");
		page.append("</th>");
		page.append("</tr>");
	}

	static void tablePourValeurApres(StringBuilder page) {
		cssTable(page);
		page.append("<table>");
		page.append("<tr>");
		page.append("<th>");
		page.append("Valeur avant opération");
		page.append("</th>");
		page.append("<th>");
		page.append("Opération");
		page.append("</th>");
		page.append("<th>");
		page.append("Valeur après opération");
		page.append("</th>");
		page.append("</tr>");
	}

}