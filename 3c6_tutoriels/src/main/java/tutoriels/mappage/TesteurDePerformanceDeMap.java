/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package tutoriels.mappage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tutoriels.core.performance_app.PerformanceTestsDriver;
import tutoriels.core.performance_app.TestParameters;
import tutoriels.validateurs.ValidateurMapJava;

public class TesteurDePerformanceDeMap extends PerformanceTestsDriver {

	private static final int NOMBRE_DE_REPETITIONS = 10000;

	@Override
	public TestParameters getTestParametersFor(String providerMethodPrefix, String methodName) {

		TestParameters testsParameters = null;

		if(methodName.equals("fairePlusieursAjoutsAleatoires")) {
			
			testsParameters = new TestParameters((int) 1E3, (int) 1E4*5, 30, 6.0, true);

		} else if(methodName.equals("fairePlusieursModificationsAleatoires")) {
			
			testsParameters = new TestParameters((int) 1E3, (int) 1E4*5, 30, 6.0, true);

		} else if(methodName.equals("fairePlusieursRetraitsAleatoires")) {
			
			testsParameters = new TestParameters((int) 1E4, (int) 1E4*5, 30, 6.0, true);

		} else if(methodName.equals("accederAuxClesDansOrdre")) {
			
			testsParameters = new TestParameters((int) 1E4, (int) 1E5, 30, 0.5, true);

		}

		return testsParameters;
	}

	@Override
	public List<Object> generateArgumentsFor(String providerMethodPrefix, String methodName, int desiredInputSize) {
		List<Object> arguments = new ArrayList<>();
		if(desiredInputSize <= 0) {
			desiredInputSize = 10;
		}

		if(methodName.equals("fairePlusieursAjoutsAleatoires")) {

			arguments.add(new HashMap<>());
			arguments.add(ValidateurMapJava.clesAleatoires(desiredInputSize));
			arguments.add(NOMBRE_DE_REPETITIONS);

		} else if(methodName.equals("fairePlusieursModificationsAleatoires") 
				|| methodName.equals("fairePlusieursRetraitsAleatoires")
				|| methodName.equals("accederAuxClesDansOrdre") ) {
			
			String[] cles = ValidateurMapJava.clesAleatoires(desiredInputSize);

			arguments.add(ValidateurMapJava.mapAleatoire(cles, desiredInputSize));
			arguments.add(cles);
			arguments.add(NOMBRE_DE_REPETITIONS);
		}
			
		/*
		} else if(methodName.equals("fairePlusieursRetraitsAleatoires")){
			
			String[] cles = ValidateurMapJava.clesAleatoires(desiredInputSize);
			Map<String, Integer> map = ValidateurMapJava.mapAleatoire(cles, desiredInputSize);


			Integer[] valeurs = new Integer[cles.length];
			for(int i = 0; i < valeurs.length; i++) {
				valeurs[i] = map.get(cles[i]);
			}

			arguments.add(map);
			arguments.add(valeurs);
			arguments.add(NOMBRE_DE_REPETITIONS);
		}
		*/

		return arguments;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Object> adaptArgumentsFor(Object providedObject, String methodName, List<Object> arguments) {
		
		List<Object> adaptedArguments;

		if(methodName.equals("fairePlusieursAjoutsAleatoires") 
				|| methodName.equals("fairePlusieursRetraitsAleatoires")
				|| methodName.equals("fairePlusieursModificationsAleatoires")) {
			
			adaptedArguments = new ArrayList<>();
			
			TesteurDeMap testeur = (TesteurDeMap) providedObject;
			
			MapJava<Cle<String>, Integer> mapAdapte = testeur.nouveauMap();
			Map<String, Integer> mapOriginal = (Map<String, Integer>) arguments.get(0);

			
			for(Map.Entry<String, Integer> entry : mapOriginal.entrySet()) {
				Cle<String> cle = testeur.nouvelleCle(entry.getKey());

				mapAdapte.put(cle, entry.getValue());
			}

			String[] clesOriginales = (String[]) arguments.get(1);
			Cle<String>[] cles = (Cle<String>[]) Array.newInstance(Cle.class, clesOriginales.length);
			for(int i = 0; i < clesOriginales.length; i++) {
				Cle<String> cle = testeur.nouvelleCle(clesOriginales[i]);
				cles[i] = cle;
			}
			
			adaptedArguments.add(mapAdapte);
			adaptedArguments.add(cles);
			adaptedArguments.add(arguments.get(2));

		/*
		} else if(methodName.equals("fairePlusieursRetraitsAleatoires")) {

			adaptedArguments = new ArrayList<>();
			
			TesteurDeMap testeur = (TesteurDeMap) providedObject;
			
			MapJava<Cle<String>, Integer> mapAdapte = testeur.nouveauMap();
			Map<String, Integer> mapOriginal = (Map<String, Integer>) arguments.get(0);

			List<Integer> valeurs = new ArrayList<>();
			
			for(Map.Entry<String, Integer> entry : mapOriginal.entrySet()) {
				Cle<String> cle = testeur.nouvelleCle(entry.getKey());

				mapAdapte.put(cle, entry.getValue());
				valeurs.add(entry.getValue());
			}
			
			adaptedArguments.add(mapAdapte);
			adaptedArguments.add(valeurs.toArray(new Integer[valeurs.size()]));
			adaptedArguments.add(arguments.get(2));
			*/

		} else if(methodName.equals("accederAuxClesDansOrdre") ) {
			
			adaptedArguments = new ArrayList<>();
			
			TesteurDeMap testeur = (TesteurDeMap) providedObject;
			
			MapJava<Cle<String>, Integer> mapAdapte = testeur.nouveauMap();
			Map<String, Integer> mapOriginal = (Map<String, Integer>) arguments.get(0);
			
			for(Map.Entry<String, Integer> entry : mapOriginal.entrySet()) {
				Cle<String> cle = testeur.nouvelleCle(entry.getKey());
				mapAdapte.put(cle, entry.getValue());
			}

			adaptedArguments.add(mapAdapte);

		}else {
			
			adaptedArguments = arguments;
		}

		return adaptedArguments;
	}

	@Override
	public List<String> desiredMethodOrder() {
		return new ArrayList<String>();
	}
}
