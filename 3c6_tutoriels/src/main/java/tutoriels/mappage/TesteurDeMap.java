/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package tutoriels.mappage;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class TesteurDeMap {
	
	private BlockingQueue<String> cles =  new LinkedBlockingQueue<>();
	
	public TesteurDeMap() {
		new Thread() {
			@Override
			public void run() {
				while(true) {
					try {
						System.out.println(cles.take());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	public abstract MapJava<Cle<String>, Integer> nouveauMap();

	public abstract Cle<String> nouvelleCle(String valeur);

	public abstract void fairePlusieursAjoutsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations);

	public abstract void fairePlusieursModificationsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations);

	public abstract void fairePlusieursRetraitsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] valeurs, int nombreOperations);

	public abstract void accederAuxClesDansOrdre(MapJava<Cle<String>, Integer> map);
	
	protected void afficherCle(String cle) {
		cles.add(cle);
	}

}
