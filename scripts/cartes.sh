nombre_de_cartes_par_defaut=6

cartes=$(test "$1" != "" && seq "$1" 2>/dev/null)

if [ "$cartes" = "" ]; then
    cartes=$(seq $nombre_de_cartes_par_defaut)
fi

numeros="2 3 4 5 6 7 8 9 10"
sortes="♡ ♢ ♧ ♤" 

for i in $cartes; 
do
    numero=$(shuf -e $numeros | head -n 1)
    sorte=$(shuf -e $sortes | head -n 1)
    carte="$numero$sorte"
    echo -n "$carte "
done

echo ""




