# Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
#
# This file is part of Ntro
#
# Ntro is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ntro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with aquiletour.  If not, see <https://www.gnu.org/licenses/>

##### INCLUDE #####
this_dir=$(readlink -f "$0")
scripts_dir=$(dirname "$this_dir")
. "$scripts_dir/include.sh"
###################

save_dir

cd "$root_dir"


git clone git@gitlab.com:mathieu-bergeron/ca.ntro.git                         ntro
git clone -b prochainMain git@gitlab.com:mathieu-bergeron/ca.ntro.git         ntro_4f5  # deviendra main a2023
git clone -b ntro_3c6 git@gitlab.com:mathieu-bergeron/ca.ntro.git             ntro_3c6

git clone git@gitlab.com:mathieu-bergeron/ca.ntro.v02.git                     v02
git clone git@gitlab.com:mathieu-bergeron/ca.ntro.tests.git                   _tests

git clone git@gitlab.com:mathieu-bergeron/ca.ntro.cards.git                   cards

cd cards
git clone git@gitlab.com:mathieu-bergeron/ca.ntro.cards.solutions             solutions    # private
git clone git@gitlab.com:mathieu-bergeron/ca.ntro.cards.enonces               enonces      # private
git clone git@gitlab.com:mathieu-bergeron/ca.ntro.cards.examens               examens      # private
cd ..

git clone git@gitlab.com:mathieu-bergeron/ca.ntro.3c6.git                     3c6

cd 3c6
git clone https://gitlab.com/mathieu-bergeron/ca.ntro.3c6.solutions.git       solutions  # private
cd ..

git -b prochainMain clone git@gitlab.com:mathieu-bergeron/ca.ntro.4f5.git     4f5  # private

restore_dir

