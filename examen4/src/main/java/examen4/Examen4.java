/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen4;

import java.lang.reflect.Method;

import ca.ntro.core.introspection.Introspector;
import ca.ntro.core.models.stores.MemoryStore;
import ca.ntro.core.system.debug.T;
import tutoriels.core.app.Atelier;
import tutoriels.core.app.CurrentExercise;
import tutoriels.core.app.InitializerExercise;
import tutoriels.core.models.reports.ReportNodeViewModel;
import tutoriels.core.models.reports.ReportViewModel;
import tutoriels.core.performance_app.PerformanceTestsDriver;
import tutoriels.liste.ListeJava;
import tutoriels.liste.TesteurDeListe;
import tutoriels.validateurs.ValidateurListeJava;

public abstract class Examen4 extends Atelier {

    static {

        new InitializerExercise().initialize(Examen4.class);
        
    }

    @Override
    protected void performValidation() {
        T.call(this);
        
        String exerciseId = CurrentExercise.getId();

        ReportViewModel validationReport = MemoryStore.get(ReportViewModel.class, CurrentExercise.getId());
        validationReport.setReportTitle(exerciseId);
        validationReport.setExpectedNumberOfSubReports(3);
        
        //addListSubReport(validationReport, "ListeFin", "fournirListeFin", false);
        addListSubReport(validationReport, "ListeDebut", "fournirListeDebut", true);
        addListSubReport(validationReport, "ListeNaive", "fournirListeNaive", true);
        //addListSubReport(validationReport, "ListeDebutFin", "fournirListeDebutfin", false);

        addListSubReport(validationReport, "ListeChaineeSimple", "fournirListeSimple", true);
        //addListSubReport(validationReport, "ListeChaineeDouble", "fournirListeDouble", false);
        addListSubReport(validationReport, "ListeHybride", "fournirListeHybride", true);
    }

    private void addListSubReport(ReportViewModel validationReport, String listName, String providerName, boolean ecrireGraphes) {
        ReportNodeViewModel subReport = ReportViewModel.newSubReport();
        validationReport.addSubReport(subReport);
        subReport.setTitle(listName);
        Method creerListeVide = Introspector.findMethodByName(this.getClass(), providerName);

        callValidator(ecrireGraphes, subReport, creerListeVide);
    }


	protected void callValidator(boolean ecrireGraphes, ReportNodeViewModel subReport, Method creerListeVide) {
		ValidateurListeJava.valider(subReport, this, creerListeVide, ecrireGraphes);
	}

    protected void checkIfDatabaseExists() {
        T.call(this);
        // XXX: nothing, we do not want a database
    }

    @Override 
    public boolean siExecutable() {return false;}

    @Override 
    public void executer() {}

    @Override
    public PerformanceTestsDriver createPerformanceTestsDriver() {
        return new TesteurExamen4();
    }


    //public abstract TesteurDeListe fournirTesteurDeListeFin();
    public abstract TesteurDeListe fournirTesteurDeListeDebut();
    //public abstract TesteurDeListe fournirTesteurDeListeDebutfin();

    public abstract TesteurDeListe fournirTesteurDeListeSimple();
    //public abstract TesteurDeListe fournirTesteurDeListeDouble();
    public abstract TesteurDeListe fournirTesteurDeListeHybride();
    public abstract TesteurDeListe fournirTesteurDeListeNaive();

    //public abstract ListeJava<Character> fournirListeFin();
    public abstract ListeJava<Character> fournirListeDebut();
    //public abstract ListeJava<Character> fournirListeDebutfin();

    public abstract ListeJava<Character> fournirListeSimple();
    //public abstract ListeJava<Character> fournirListeDouble();
    public abstract ListeJava<Character> fournirListeHybride();
    public abstract ListeJava<Character> fournirListeNaive();

}
