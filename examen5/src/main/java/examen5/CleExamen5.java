/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen5;

import tutoriels.mappage.Cle;

public class CleExamen5 extends Cle<String> {

	public CleExamen5(String valeur) {
		super(valeur);
	}

	private int indiceLettre(char lettre) {
		return ((int) lettre) - 97;
	}

	public int indicePremiereLettre() {
		char premiereLettre = 0;
		
		if(!getValeur().isEmpty()) {
			
			premiereLettre = getValeur().charAt(0);
			
		}

		return indiceLettre(premiereLettre);
	}
	
	public boolean siCleVide() {
		return getValeur().isEmpty();
	}

	public CleExamen5 cleSuivante() {
		CleExamen5 cleSuivante = null;

		if(getValeur().length() > 1) {

			cleSuivante = new CleExamen5(getValeur().substring(1));

		}else {
			
			cleSuivante = new CleExamen5("");
		}

		return cleSuivante;
	}

}
