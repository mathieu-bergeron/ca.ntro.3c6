/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen5;

public class NoeudReference<C extends Comparable<C>> extends Noeud<C> {

	private C valeur;
	private Noeud<C> enfantGauche;
	private Noeud<C> enfantDroit;

	private transient Noeud<C> parent;

	public NoeudReference(C valeur) {
		this.valeur = valeur;
	}

	public NoeudReference(C valeur, Noeud<C> parent) {
		this.valeur = valeur;
		this.parent = parent;
	}

	@Override
	public Noeud<C> parent() {
		return parent;
	}
	
	@Override
	public Noeud<C> enfantGauche() {
		return enfantGauche;
	}

	@Override
	public Noeud<C> enfantDroit() {
		return enfantDroit;
	}

	@Override
	public C valeur() {
		return valeur;
	}

	@Override
	public void inserer(C valeur) {

		if(valeur.compareTo(valeur()) < 0) {
			
			insererVersLaGauche(valeur);
			
		}else if(valeur.compareTo(valeur()) > 0) {
			
			insererVersLaDroite(valeur);

		}else {

			this.valeur = valeur;

		}
	}

	private void insererVersLaGauche(C valeur) {

		if(enfantGauche() == null) {
			
			setEnfantGauche(new NoeudReference<C>(valeur,this));
			equilibrer();

		}else {
			
			enfantGauche().inserer(valeur);
			
		}
	}

	private void insererVersLaDroite(C valeur) {
		if(enfantDroit() == null) {
			
			setEnfantDroit(new NoeudReference<C>(valeur,this));
			equilibrer();
			
		}else {
			
			enfantDroit().inserer(valeur);
		}
	}

	@Override
	protected void setValeur(C valeur) {
		this.valeur = valeur;
	}

	@Override
	protected void setEnfantGauche(Noeud<C> enfantGauche) {
		this.enfantGauche = enfantGauche;
	}

	@Override
	protected void setEnfantDroit(Noeud<C> enfantDroit) {
		this.enfantDroit = enfantDroit;
	}

	@Override
	protected void setParent(Noeud<C> parent) {
		this.parent = parent;
	}

	@Override
	protected Noeud<C> nouveauNoeud(C valeur, Noeud<C> parent) {
		return new NoeudReference<C>(valeur, parent);
	}

	@Override
	protected Noeud<C> nouveauNoeud(C valeur) {
		return new NoeudReference<C>(valeur);
	}
}
