/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tutoriels.mappage.Cle;
import tutoriels.mappage.MapJava;
import tutoriels.mappage.TesteurDeMap;

public abstract class TesteurDeMapAbstrait extends TesteurDeMap {
	
	private static final Random alea = new Random();

	@Override
	public void fairePlusieursAjoutsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < cles.length; i++) {
			map.put(cles[i],alea.nextInt(cles.length));
		}
	}

	@Override
	public void fairePlusieursModificationsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < cles.length; i++) {
			map.put(cles[i],alea.nextInt(cles.length));
		}
	}

	@Override
	public void fairePlusieursRetraitsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < cles.length; i++) {
			map.remove(cles[i]);
		}
	}

	@Override
	public CleExamen5 nouvelleCle(String valeur) {
		return new CleExamen5(valeur);
	}

	@Override
	public void accederAuxClesDansOrdre(MapJava<Cle<String>, Integer> map) {
		List<Cle<String>> cles = map.keys();
		List<String> valeurCles = new ArrayList<>();

		for(Cle<String> cle : cles) {
			System.out.println("EXAMEN: " + cle.getValeur());
		}
	}


}
