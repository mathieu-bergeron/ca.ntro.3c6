/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen3b;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import tutoriels.core.performance_app.PerformanceTestsDriver;
import tutoriels.core.performance_app.TestParameters;

public class TesteurExamen3b extends PerformanceTestsDriver {

	public static final Random alea = new Random(new Date().getTime());
	public static final char[] alphabet = new char[] {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t', 'u', 'v','w','x', 'y', 'z'};

	@Override
	public TestParameters getTestParametersFor(String providerMethodPrefix, String methodName) {

		TestParameters testsParameters = null;

		if(methodName.equals("motsDebutantPar")) {
			
			testsParameters = new TestParameters((int) 1E1, (int) 1E3, 30, 16.0, true);


		} else if(methodName.equals("motsAvecLaMemePremiereLettre")) { 

			testsParameters = new TestParameters((int) 1E1, (int) 1E3, 30, 16.0, true);

		} else if(methodName.equals("toutesLesPhrasesDeDeuxMots")) { 

			testsParameters = new TestParameters((int) 1E2, (int) 1E3, 30, 40.0, true);

		}

		return testsParameters;
	}

	@Override
	public List<Object> generateArgumentsFor(String providerMethodPrefix, String methodName, int desiredInputSize) {
		List<Object> arguments = new ArrayList<>();

		if(methodName.equals("motsDebutantPar")) {

			arguments.add(TesteurExamen3b.listeAleatoire(desiredInputSize, 3));
			arguments.add(TesteurExamen3b.lettreAleatoire());

		} else if(methodName.equals("motsAvecLaMemePremiereLettre")
				|| methodName.equals("toutesLesPhrasesDeDeuxMots")) {

			arguments.add(TesteurExamen3b.listeAleatoire(desiredInputSize, 3));
		} 

		return arguments;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Object> adaptArgumentsFor(Object providedObject, String methodName, List<Object> arguments) {
		
		return arguments;
	}

	@Override
	public List<String> desiredMethodOrder() {
		return new ArrayList<String>();
	}

	public static List<String> listeAleatoire(int taille, int tailleChaine) {
		
		List<String> liste = new ArrayList<>();
		
		for(int i = 0; i < taille; i++) {
			
			liste.add(chaineAleatoire(tailleChaine));
		}
		
		return liste;
	}

	public static String chaineAleatoire(int taille) {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < taille; i++) {
			builder.append(lettreAleatoire());
		}
		
		return builder.toString();
	}

	public static char lettreAleatoire() {
		return alphabet[alea.nextInt(alphabet.length)];
	}

	public static void afficherTableau(String[] tab) {
		System.out.print("[");
		if(tab.length > 0) {
			System.out.print(tab[0]);
		}
		for(int i = 1; i < tab.length; i++) {
			System.out.print(", " + tab[i]);
		}

		System.out.println("]");
		
	}
}
