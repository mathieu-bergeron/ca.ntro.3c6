package ca.ntro.core;

import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.core.models.stores.LocalStore;
import ca.ntro.core.services.Logger;
import ca.ntro.core.services.ReflectionService;
import ca.ntro.ntro_app_fx_impl.NtroServerJdkImpl;
import ca.ntro.ntro_core_impl.graph_writer.GraphWriter;
import ca.ntro.ntro_core_impl.services.GraphWriterJdk;
import ca.ntro.ntro_core_impl.services.LoggerImpl;
import ca.ntro.ntro_core_impl.services.ReflectionServiceJdk;
import ca.ntro.ntro_core_impl.services.StackAnalyzerJdk;
import ca.ntro.ntro_core_impl.values.CodeLocation;

public class UnitializedNtro {
	
	private static Logger logger = new LoggerImpl();
	private static ReflectionService reflectionService = new ReflectionServiceJdk();
	private static GraphWriter graphWriter = new GraphWriterJdk();
	
	public static String projectPath() {
		return NtroServerJdkImpl.instance().options().projectPath();
	}

	public static void minimalInitialization() {
		CodeLocation appLocation = (new StackAnalyzerJdk()).callerLocation();
		NtroServerJdk.resolveProjectPathIfNeeded(appLocation);
		NtroServerJdk.registerOptions();
		LocalStore.open();
	}

	public static Logger logger() {
		return logger;
	}
	
	public static ReflectionService reflection() {
		return reflectionService;
	}

	public static GraphWriter graphWriter() {
		return graphWriter;
	}

}
