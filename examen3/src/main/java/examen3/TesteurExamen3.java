/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package examen3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import tutoriels.core.performance_app.PerformanceTestsDriver;
import tutoriels.core.performance_app.TestParameters;
import tutoriels.liste.ListeJava;
import tutoriels.liste.TesteurDeListe;

public class TesteurExamen3 extends PerformanceTestsDriver {
		
	private static final Random alea = new Random();

	@Override
	public TestParameters getTestParametersFor(String providerMethodPrefix, String methodName) {
		TestParameters testsParameters = null;
		
		// défaut
		testsParameters = new TestParameters((int) 1E1, (int) 1E2, 30, 10.0, true);

		if(methodName.equals("puissance_log")) {

			testsParameters = new TestParameters((int) 1E2, (int) 1E8, 30, 2.0, true);

		} else if(methodName.equals("puissance_lin")) {

			testsParameters = new TestParameters((int) 1E4, (int) 1E6, 30, 2.0, true);

		} else if(methodName.equals("puissance_quad")) {

			testsParameters = new TestParameters((int) 1E3, (int) 1E5, 30, 10.0, true);
			//testsParameters = new TestParameters((int) 1E2, (int) 1E3, 30, 10.0, true);

		} else if(methodName.equals("puissance_exp")) {

			testsParameters = new TestParameters(2, 44, 30, 100.0, true);
			//testsParameters = new TestParameters(2, 3, 1, 100.0, true);

		} 

		return testsParameters;
	}

	@Override
	public List<Object> generateArgumentsFor(String providerMethodPrefix, String methodName, int desiredInputSize) {

		// XXX: par défaut
		List<Object> arguments = List.of(desiredInputSize);

		if(methodName.equals("puissance_log")
				|| methodName.equals("puissance_lin")
				|| methodName.equals("puissance_quad")
				|| methodName.equals("puissance_exp")) {
			
			arguments = List.of(desiredInputSize, desiredInputSize);
		} 

		return arguments;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> adaptArgumentsFor(Object providedObject, String methodName, List<Object> arguments) {
		
		List<Object> adaptedArguments = List.copyOf(arguments);

		return adaptedArguments;
	}

	@Override
	public List<String> desiredMethodOrder() {
		return List.of("puissance_log", "puissance_lin", "puissance_quad", "puissance_exp");
	}

	
}
