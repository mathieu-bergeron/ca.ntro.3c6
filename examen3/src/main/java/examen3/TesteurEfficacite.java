package examen3;

public interface TesteurEfficacite {
	
	
	long puissance_log(long base, long exposant);
	long puissance_lin(long base, long exposant);
	long puissance_quad(long base, long exposant);
	long puissance_exp(long base, long exposant);
	//void lineaire01(int n);
	//void quadratique01(int n);

}
