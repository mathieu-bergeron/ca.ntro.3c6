src=$1
dst=$2

if [ "$1" = "" -o "$2" = "" ]; then
    echo "usage $0 ancien_nom nouveau_nom"
    exit 0
fi

renommer(){

    files=$(find . -type f -name "$src.*")
    for file in $files; 
    do
        filename=$(basename $file)
        mv -v $file "$(dirname $file)/$dst.${filename#*.}"
    done

    files=$(find . -type f -name "Mon$src.*")
    for file in $files; 
    do
        filename=$(basename $file)
        mv -v $file "$(dirname $file)/Mon$dst.${filename#*.}"
    done

    ag "$src" -l | while read i; do sed "s/$src/$dst/g" -i "$i"; done

}

renommer
cd solutions
renommer
